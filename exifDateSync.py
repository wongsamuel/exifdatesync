import os,sys
import exifread
import argparse
from datetime import datetime


EXIF_DATE='EXIF DateTimeOriginal'

#
# getExif(filename)
#   filename - name of the file to extract exif data
#   return exif tags->dicts
#
def getExifTags(filename):
    # Open image file for reading (binary mode)
    with open(filename, 'rb') as f:
        # Return Exif tags
        tags = exifread.process_file(f)

        #for tag in tags.keys():
            #if tag not in ('JPEGThumbnail', 'TIFFThumbnail', 'Filename', 'EXIF MakerNote'):
                #print ("Key: %s, value %s" % (tag, tags[tag]))
                #"Image DateTime"
                #"EXIF DateTimeOriginal"
            

    return tags

#
# getDateFromExifTags(tags)
# tags - tags returned from getExif()
# return exif capture date -> datetime 
#
def getDateFromExifTags(tags):
    if EXIF_DATE in tags:
        dateStr=str(tags[EXIF_DATE])
        #print(dateStr)
        #print(type(dateStr))
        date_obj = datetime.strptime(dateStr, '%Y:%m:%d %H:%M:%S')
        #print(date_obj)
        return(date_obj)



def setFileDate(filename, ts):
    os.utime(filename, times=(ts,ts))

def main():
    parser = argparse.ArgumentParser(description='Sync file date with EXIF date')
    parser.add_argument('filename', 
                        help='Filename to be processed')
    # parser.add_argument('--sum', dest='accumulate', action='store_const',
    #                     const=sum, default=max,
    #                     help='sum the integers (default: find the max)')

    args = parser.parse_args()
    filename=args.filename
    print(f"Reading EXIF date from {filename}")
    tags=getExifTags(filename)
    if not tags:
        print ("Abort. EXIF data does not exist")
        sys.exit(1)

    exif_date=getDateFromExifTags(tags)
    if not exif_date:
        print ("Abort. EXIF capture date not exist")
        sys.exit(1)
    print(f"EXIF capture date: {exif_date}")

    orgDate=datetime.fromtimestamp(os.path.getmtime(filename))
    print(f"Original file mod date: {orgDate}")

    setFileDate(filename,exif_date.timestamp())
    newDate=datetime.fromtimestamp(os.path.getmtime(filename))
    print(f"Updated file mod date: {newDate}")

    basename=os.path.splitext(filename)[0]
    for ext in [".MOV"]:
        mov_filename=basename+ext
        if os.path.exists(mov_filename):
            print(f"{mov_filename} exists.  Updating file mod date as well")
            orgDate=datetime.fromtimestamp(os.path.getmtime(mov_filename))
            print(f"Original file mod date: {orgDate}")

            setFileDate(mov_filename,exif_date.timestamp())
            newDate=datetime.fromtimestamp(os.path.getmtime(mov_filename))
            print(f"Updated file mod date: {newDate}")






if __name__=='__main__':
    main()