
# README #

This is a tool that is to sync file modification date of a photo to the exif date that's upload from iphone.

  
### How do I get set up? ###

* The tool is written in python and it takes a file as the input

* To support photo with live view, the tool will also update .mov file with the same file name

* The required python modules are listed in pipfile

* It is recommended to use pipenv to setup the python environment:
```
# pipenv install
# pipenv shell
```
* To sync a photo:
```
# python exifDateSync.py /path/photo.jpg
```  
 * Use the shell script wrapper to sync multiple files:
 ```
 exifDateSync.sh /photoPath/IMG*.HEIC
```

### Supported file format ###

* JPEG
* HEIC
* TIFF

### Who do I talk to? ###

* Repo owner