#!/bin/sh
#
#  Usage Example: 
#  ./exifDateSync.sh /Volumes/homes/me/Camera\ Uploads/IMG_*.HEIC
# 
#echo $#
if [ $# -lt 1 ]; then
  echo "Usage: exifDateSync files"
  exit 1
fi

for file in "$@"; do
  python exifDateSync.py "${file}"
done
